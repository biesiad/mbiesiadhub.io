# Open Source Friday by GitHub

* [Open Source Friday](https://opensourcefriday.com/users/mbiesiad)

# Docs

* [ava-docs](https://github.com/mbiesiad/ava-docs/blob/pl_PL/pl_PL/readme.md)

* [storybook](https://github.com/mbiesiad/storybook/tree/pl_PL) [WIP]

# Tutorials & guidelines

* [js-stack-from-scratch](https://github.com/mbiesiad/js-stack-from-scratch)

* [nodebestpractices](https://github.com/mbiesiad/nodebestpractices/blob/master/README.polish.md)

* [Front-End-Performance-Checklist](https://github.com/mbiesiad/Front-End-Performance-Checklist)

* [front-end-interview-handbook](https://github.com/mbiesiad/front-end-interview-handbook/blob/master/Translations/Polish/README.md)

* [javascript-testing-best-practices](https://github.com/mbiesiad/javascript-testing-best-practices/blob/master/readme-pl.md)

* [awesome-fullstack-tutorials](https://github.com/mbiesiad/awesome-fullstack-tutorials/tree/pl) [WIP]

* [frontend-guidelines](https://github.com/mbiesiad/frontend-guidelines)

# Tips

* [Git-tips](https://github.com/mbiesiad/tips)

* [developer-roadmap](https://github.com/mbiesiad/developer-roadmap/tree/master/translations/polish)

# Awesome projects

* [awesome-astronomy](https://github.com/mbiesiad/awesome-astronomy)

* [awesome-chess](https://github.com/mbiesiad/awesome-chess)

* [awesome-sql](https://github.com/mbiesiad/awesome-sql)

* [COVID-19-World-Data](https://github.com/mbiesiad/COVID-19-World-Data)

* [nasa-comets](https://github.com/mbiesiad/nasa-comets)

## Enjoy !

:rocket:
